package com.m2iformation.service

import com.m2iformation.parser.Driver
import com.m2iformation.persister.support.SerializationSupport
import org.scalatra.{NotFound, Ok}

/**
  * Created by walid_mellouli
  */
class DriversRoute extends RestServlet with SerializationSupport {

  val inputFile = "/tmp/drivers"

  lazy val drivers = deserialize[Driver](inputFile)

  val exposedDrivers: Map[String, DriverRest] = getExposedDrivers(drivers.toList)

  //---- Drivers routes -----

  get("/drivers/id/:driverId") {

    val id = params("driverId")

    exposedDrivers.get(id) match {
      case Some(driver) => Ok(driver)
      case None => NotFound(s"pas trouve $id")
    }

  }

  get("/drivers") {

    exposedDrivers.values

  }

  def getExposedDrivers(drivers: List[Driver]) : Map[String, DriverRest]  = {

    val driversMap: Map[(String, String), List[Driver]] = drivers.groupBy(driver => (driver.id, driver.name))

    driversMap.map { case ((id, name), drivers) =>
      val weeks: List[DriverWeek] = drivers.map(driverWeek => DriverWeek(driverWeek.week, driverWeek.hours, driverWeek.miles))
      (id, DriverRest(id, name, weeks))
    }
  }

}

case class DriverWeek(week: Int, hours: Int, miles: Int)
case class DriverRest(id: String, name: String, weeks: List[DriverWeek])

