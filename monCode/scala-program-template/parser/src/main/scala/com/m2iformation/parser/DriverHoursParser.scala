package com.m2iformation.parser


import scala.util.{Failure, Success, Try}
import scala.xml.XML

/**
  * Created by walid_mellouli
  */
class DriverHoursParser extends Parser {

  override type Out = DriverHours

  override def transform(line: String): Try[DriverHours] = Try {
    val xmlLine = XML.loadString(line)
    DriverHours(
      id = xmlLine.child(0).text,
      week = xmlLine.child(1).text.toInt,
      hours = xmlLine.child(2).text.toInt
    )
  }

}

case class DriverHours(id: String, week: Int, hours: Int)


