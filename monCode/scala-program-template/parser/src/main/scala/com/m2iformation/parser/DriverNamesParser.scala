package com.m2iformation.parser

import play.api.libs.json._

import scala.util.{Failure, Success, Try}


/**
  * Created by walid_mellouli
  */
class DriverNamesParser extends Parser {

  override type Out = DriverName

  override def transform(line: String): Try[DriverName] = Try {
      val jsonLine = Json.parse(line)
       DriverName(
        id = (jsonLine \ "id").as[String],
        name = (jsonLine \ "name").as[String]
      )

  }

}

case class DriverName(id: String, name: String)



