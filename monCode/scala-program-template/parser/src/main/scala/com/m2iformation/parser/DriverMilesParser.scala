package com.m2iformation.parser

import scala.util.{Failure, Success, Try}

/**
  * Created by walid_mellouli
  */
class DriverMilesParser extends Parser {

  override type Out = DriverMiles

  override def transform(line: String): Try[DriverMiles] = Try {
      val csvFields = line.split(",")
      DriverMiles(
        id = csvFields(0),
        week = csvFields(1).toInt,
        miles = csvFields(2).toInt
      )
    }



}

case class DriverMiles(id: String, week: Int, miles: Int)
