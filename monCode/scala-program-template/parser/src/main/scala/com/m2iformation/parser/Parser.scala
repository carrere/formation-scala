package com.m2iformation.parser

import java.io.{BufferedReader, IOException, InputStream, InputStreamReader}

import com.sun.net.httpserver.Authenticator
import org.scalactic.Fail
import org.slf4j.LoggerFactory

import scala.io.Source
import scala.util.{Failure, Success, Try}

/**
  * Mapping lines
  */
trait Parser {

  type Out

  private val logger = LoggerFactory.getLogger(this.getClass.getName)

  /**
    * transform a line to output data
    *
    * @param line to transform
    * @return
    */
  def transform(line: String): Try[Out]

  /**
    * Parse an InputStream into a list of output objects
    *
    * @param input
    * @param skipHeader either to skip header or not (useful for csv files)
    * @return
    */
  def parseInputStream(input: InputStream, skipHeader: Boolean = false): List[Out] = {

    val toDrop = if (skipHeader) 1 else 0

   Source
      .fromInputStream(input)
      .getLines()
      .toList
      .drop(toDrop)
      .flatMap(
        line => transform(line) match {
          case Success(parsed) => Some(parsed)
          case Failure(e) => {
            logger.error(s"Line $line erroneous", e)
            None
          }
        }
      )
 }

}

