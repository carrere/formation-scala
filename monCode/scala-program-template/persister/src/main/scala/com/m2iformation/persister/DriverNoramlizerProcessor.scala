package com.m2iformation.persister

import java.io.InputStream

import com.m2iformation.parser.ParserBoot.serialize
import com.m2iformation.parser._
import com.m2iformation.persister.support.SerializationSupport


/**
  * Created by walid_mellouli
  */
class DriverNoramlizerProcessor extends SerializationSupport {

  //parsers
  private val milesParser = new DriverMilesParser
  private val hoursParser = new DriverHoursParser
  private val namesParser = new DriverNamesParser

  def run(milesInput: InputStream, hoursInput: InputStream, namesInput: InputStream): List[Driver] = {
    //parse input streams
    val driverMilesList: List[DriverMiles] = milesParser.parseInputStream(milesInput, skipHeader = true)
    val driverHoursList: List[DriverHours] = hoursParser.parseInputStream(hoursInput, skipHeader = false)
    val driverNameList: List[DriverName] = namesParser.parseInputStream(namesInput, skipHeader = false)

    val driversWithMiles: List[Driver] = enrichDriverWithMiles(driverNameList, driverMilesList)
    val drivers: List[Driver] = enrichDriverWithHours(driversWithMiles, driverHoursList)

    drivers
  }

  private def enrichDriverWithMiles(driverNameList: List[DriverName], driverMilesList: List[DriverMiles]): List[Driver] = {

    val driverNameMap: Map[String, String] = driverNameList.map(driver => (driver.id, driver.name)).toMap
    val drivers = driverMilesList.map(
      driverMiles => new Driver(
        id = driverMiles.id,
        miles = driverMiles.miles,
        week = driverMiles.week,
        hours = 0,
        name = driverNameMap.get(driverMiles.id).getOrElse("NULL"))
    )
    drivers

  }

  private def enrichDriverWithHours(drivers: List[Driver], driverHoursList: List[DriverHours]): List[Driver] = {
    val driverHoursMap = driverHoursList.map(driverHours => ((driverHours.id, driverHours.week), driverHours.hours)).toMap
    val enrichedDrivers = drivers.map(driver => driver.copy(hours = driverHoursMap.get((driver.id, driver.week)).getOrElse(0)))
    enrichedDrivers

  }
}
