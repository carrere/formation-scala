package com.m2i.modulea

object ModuleABoot extends App {

  def now():Long = System.currentTimeMillis()
    // CallByValue (time est evalue a l'appel de process et aura donc la meme valeur lors des 3 appels
    //  def process(a: Int, b:Int, c:Int, time: Long) =
    // CallByName (time est evalue quand on l'utilise, donc sa valeur va changer aux 3 appels
    def process(a: Int, b:Int, c:Int, time: => Long) =
  {
    println(s"First step: process $a")
    Thread.sleep(100)
    println(s"Time $time")
    println(s"Second step: process $b")
    Thread.sleep(100)
    println(s"Time $time")
    println(s"Third step: process $c")
    Thread.sleep(100)
    println(s"Time $time")

  }

def factoriel(x:Int):Long = { if (x == 0) 1 else x * factoriel(x -1)}
def pairOuImpair(x: Int): String = {if (x % 2 == 0) "pair" else "impair"}
def intToString(x: Int, transfo: Int => String) = println(transfo(x))


  //process(1,2,3, now())
  //intToString(4, pairOuImpair)

}
