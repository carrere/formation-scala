package com.m2i.modulea

import java.util.Currency

case class Money (value:Int, currency:String)

object Money {

  def addMoneys(m1: Money, m2: Money): Option[Money] = if (m1.currency != m2.currency) None else Some(Money(m1.value + m2.value, m1.currency))

  def sum(ms: List[Money], currency: String): Money = {
    val filteredMoney = ms.filter(m => m.currency.equals(currency))
    Money(filteredMoney.foldLeft(0) { case (acc, money) => acc + money.value }, currency)
  }

  def sumMoneyByCurrency(ms: List[Money]): List[Money] = {

    val groupByCurrency: Map[String, List[Money]] = ms.groupBy(money => money.currency)
    val sumByCurr: List[Money] = groupByCurrency
      .map { case (currency, ms) => sum(ms, currency) }
      .toList
    sumByCurr

  }

}



  object MoneyMain extends App {
    val ms = List(Money(3, "€"), Money(3, "$"), Money(15, "€"), Money(1, "$"))


    val currencyErrorMessage = "on melange pas les currency"
    val addEuros = Money.addMoneys(ms(0), ms(2)).getOrElse(throw new RuntimeException(currencyErrorMessage))
    println(s"on somme des euros: $addEuros")

    try {
      val addChouxCarottes = Money.addMoneys(ms(0), ms(1)).getOrElse(throw new RuntimeException(currencyErrorMessage))
      println(s"on melange dollars et euros $addChouxCarottes")

    } catch {
      case _: Throwable => println(currencyErrorMessage)
    }

    val sumEuros = Money.sum(ms, "€")
    println(s"Euros = $sumEuros")

    val sumDollars = Money.sum(ms, "$")
    println(s"Dollars = $sumDollars")

    println(Money.sumMoneyByCurrency(ms))
  }
