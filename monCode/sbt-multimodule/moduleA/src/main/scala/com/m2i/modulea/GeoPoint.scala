package com.m2i.modulea

class GeoPoint (val x:Int, val y:Int) {

    def move(dx:Int, dy:Int): GeoPoint ={
      new GeoPoint(x + dx, y +dy)
    }



  override def toString:String = s"GeoPoint($x, $y)"
}

object MainGeoPoint extends App {
  val p = new GeoPoint(5, 2)
  val q = p.move(2, 2)
  println(p)
  println(q)

  val numList: List[Int] = List(1, 2, 3, 4, 5)

  println(numList ++ numList.reverse)
  println(numList.head)


  def produit(xs: List[Int]): Int = xs.reduce {(a,b) => a * b}
  println(numList.map(x => x * x))
  case class Person(name: String, age: Int)
  val persons = List(Person("bob",12), Person("Ed", 8), Person("Michel", 10))
  val olderpersons = persons.map(person => Person (person.name, person.age +1))
  println(olderpersons)
  println(olderpersons.filter(person => person.age > 10))

}
