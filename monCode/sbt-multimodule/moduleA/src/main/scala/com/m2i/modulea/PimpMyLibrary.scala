package com.m2i.modulea

import org.joda.time.DateTime


object ImprovedJodaTimeObject {
  implicit class ImprovedJodaTime(d: DateTime){
    def between(d1:DateTime, d2:DateTime):Boolean = {
      if (( d.isAfter(d1) && d.isBefore(d2) ) || ( d.isAfter(d2) && d.isBefore(d1)))
        true
      else
        false
    }
  }

  def isAfter(d1: DateTime): Boolean = true
}



object PimpMyLibraryMain extends App{

  import ImprovedJodaTimeObject._
  val today: DateTime = DateTime.now()
  val tomorrow: DateTime = today.plusDays(1)
  val yesterday: DateTime = today.minusDays(1)
  println(today.isAfter(yesterday))
  println(today.isAfter(tomorrow))
  println(today.between(yesterday,tomorrow))

  println(today.between(tomorrow,yesterday))
  println(tomorrow.between(today,yesterday))
}
