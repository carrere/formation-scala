package com.m2i.modulea

sealed abstract class Animal

case class Cheval(pattes: Int, poils: Boolean) extends Animal
case class Chien(pattes: Int, poils: Boolean) extends Animal
case class Canard(pattes: Int, poils: Boolean) extends Animal
case class Capybara(pattes: Int, poils: Boolean) extends Animal


object Person {
  def isComestible(animal: Animal): Boolean = animal match {
    case Cheval(pattes, poils) => true
    case Chien(pattes, poils) => false
    case Canard(pattes, poils) => true
    case _ => throw new RuntimeException(s"connait pas cet animal, goute $animal")
  }
}


object Main extends App{

  val johnnyJumper = Cheval(4, true)
  val laika = Chien(4, true)
  val duffy = Canard(2, false)
  val bob =  Capybara(4, true)

  println(Person.isComestible(laika))
  println(Person.isComestible(duffy))
  println(Person.isComestible(johnnyJumper))


}
