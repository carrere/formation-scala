package com.m2i.search.pattern

import scala.io.Source
import scala.util.matching.Regex


/**
  * Search for patterns in files
  * Use:
  *   search-pattern -i --pattern gers --files /home/formation12/file1.txt,/home/formation12/file2.txt
  *   search-pattern -i -r --pattern '^[\d]{2,}.*$' --files .../file1.txt,.../file2.txt
  */
class SearchPatternProcessor {

  def searchPattern(files: List[String], pattern: String, regex: Boolean)(implicit ignoreCase: Boolean): List[FileLine] = {


    files.flatMap(file => {
      Source.fromFile(file)
        .getLines()
        .toList
        .zipWithIndex
        .filter {
          case (line, index) => regex match {
            case true => {
              val re = new Regex(pattern.replaceAll("'",""))
              val found = re.findFirstIn(withCase(line))
              found.isDefined
            }
            case false =>  withCase(line).contains(withCase(pattern))
          }
        }
        .map { case (line, index) => FileLine(file, line, index +1) }
    })
  }

  private def withCase(line: String)(implicit ignoreCase: Boolean): String = if (ignoreCase) line.toLowerCase else line

  private def boolToOption(expression: Boolean, value: FileLine): Option[FileLine] = {
    if (expression) Some(value) else None
  }

}

case class FileLine(file: String, line: String, nbr: Int) {
  override def toString: String = s"[FILE] $file:$nbr [LINE] $line"
}
