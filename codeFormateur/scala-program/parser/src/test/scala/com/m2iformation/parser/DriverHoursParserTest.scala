package com.m2iformation.parser

import java.io.ByteArrayInputStream

import org.scalatest.{FlatSpec, GivenWhenThen, Matchers}

/**
  * Created by walid_mellouli
  */
class DriverHoursParserTest
  extends FlatSpec
    with Matchers
    with GivenWhenThen {

  val parser = new DriverHoursParser()

  "transform" should "parse a line and return a success for valid line" in {
    val line = "<driver-hours><id>10</id><week>1</week><hours>70</hours></driver-hours>"
    val parsed = parser.transform(line)
    parsed.isSuccess shouldBe true
    parsed.get shouldBe DriverHours("10", 1, 70)
  }

  it should "return a fail if line is corrupted" in {
    val line = "<driver-hk>1</week><hours>70</hours></driver-hours>"
    val parsed = parser.transform(line)
    parsed.isFailure shouldBe true
  }


  "parseInputStream" should "parse all lines for valid input" in {
    Given("an input stream with only valid lines")
    val input =
      new ByteArrayInputStream("""
                                 |<driver-hours><id>10</id><week>1</week><hours>70</hours></driver-hours>
                                 |<driver-hours><id>10</id><week>2</week><hours>60</hours></driver-hours>
                                 |<driver-hours><id>11</id><week>3</week><hours>80</hours></driver-hours>
                               """.stripMargin.getBytes)

    When("parse input stream")
    val results = parser.parseInputStream(input, skipHeader = false)

    Then("return all lines")
    val expected = List(
      DriverHours("10", 1, 70),
      DriverHours("10", 2, 60),
      DriverHours("11", 3, 80)
    )
    results should contain theSameElementsAs(expected)
  }

  it should "parse only valid lines and reject corrupted lines" in {
    Given("an input stream with valid and corrupted lines")
    val input =
      new ByteArrayInputStream("""
                                 |<driver-hours><id>10</id><week>1</week><hours>70</hours></driver-hours>
                                 |<driverours></driver-hours>
                                 |<driver-hours><id>11</id><week>3</week><hours>80</hours></driver-hours>
                               """.stripMargin.getBytes)

    When("parse input stream")
    val results = parser.parseInputStream(input, skipHeader = false)

    Then("return only valid lines")
    val expected = List(
      DriverHours("10", 1, 70),
      DriverHours("11", 3, 80)
    )
    results should contain theSameElementsAs(expected)
  }

  it should "return empty if only corrupted lines" in {
    Given("an input stream with valid and corrupted lines")
    val input =
      new ByteArrayInputStream("""
                                 |<drivedriver-hours>
                                 |<driverours></driver-hours>
                                 |<drive></driver-hours>
                               """.stripMargin.getBytes)

    When("parse input stream")
    val results = parser.parseInputStream(input, skipHeader = false)

    Then("return empty")
    results should contain theSameElementsAs(Nil)
  }

}
