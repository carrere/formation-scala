package com.m2iformation.parser

import scala.util.Try
import scala.xml.XML

/**
  * Created by walid_mellouli
  */
class DriverHoursParser extends Parser {

  import DriverHoursParser._

  override type Out = DriverHours

  override def transform(line: String): Try[DriverHours] = Try {
    val xml = XML.loadString(line)
    DriverHours(
      id = xml.child(IdIndex).text,
      week = xml.child(WeekIndex).text.toInt,
      hours = xml.child(HoursIndex).text.toInt
    )
  }

}

case class DriverHours(id: String, week: Int, hours: Int)

object DriverHoursParser {

  val NbFields = 3

  val IdIndex = 0
  val WeekIndex = 1
  val HoursIndex = 2

  val IdField = "id"
  val WeekField = "week"
  val HoursField = "hours"

}

