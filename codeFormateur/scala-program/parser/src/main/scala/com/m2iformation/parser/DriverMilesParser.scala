package com.m2iformation.parser

import scala.util.Try

/**
  * Created by walid_mellouli
  */
class DriverMilesParser extends Parser {

  import DriverMilesParser._

  override type Out = DriverMiles

  val separator = "\\,"

  override def transform(line: String): Try[DriverMiles] = Try {
    val elems = line.split(separator, -1)
    DriverMiles(
      id = elems(IdIndex),
      week = elems(WeekIndex).toInt,
      miles = elems(MilesIndex).toInt
    )
  }

}

case class DriverMiles(id: String, week: Int, miles: Int)

object DriverMilesParser {

  val NbFields = 3

  val IdIndex = 0
  val WeekIndex = 1
  val MilesIndex = 2

}

