package com.m2iformation.parser

import java.io.{BufferedReader, IOException, InputStream, InputStreamReader}

import org.slf4j.LoggerFactory

import scala.io.Source
import scala.util.{Failure, Success, Try}

/**
  * Mapping lines
  */
trait Parser {

  type Out

  private val logger = LoggerFactory.getLogger(this.getClass.getName)

  /**
    * transform a line to output data
    *
    * @param line to transform
    * @return
    */
  def transform(line: String): Try[Out]

  /**
    * Parse an InputStream into a list of output objects
    *
    * @param input
    * @param skipHeader either to skip header or not (useful for csv files)
    * @return
    */
  def parseInputStream(input: InputStream, skipHeader: Boolean = false): List[Out] = {
    logger.info(s"Start parser ${this.getClass.getName}")

    val lines = if (skipHeader) {
      Source.fromInputStream(input).getLines().toList.tail
    } else {
      Source.fromInputStream(input).getLines().toList
    }

    lines
      .map(transform)
      .flatMap { case (value: Try[Out]) ⇒ {
        value match {
          case Success(v) ⇒ Some(v)
          case Failure(e) ⇒ {
            logger.warn(e.getMessage)
            None
          }
        }
      }
      }

  }

}

