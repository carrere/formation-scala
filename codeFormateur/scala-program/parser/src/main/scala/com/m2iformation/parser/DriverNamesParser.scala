package com.m2iformation.parser

import play.api.libs.json._

import scala.util.Try


/**
  * Created by walid_mellouli
  */
class DriverNamesParser extends Parser {

  override type Out = DriverName

  override def transform(line: String): Try[DriverName] = Try {
    val json = Json.parse(line)
    DriverName(
      id = (json \ "id").as[String],
      name = (json \ "name").as[String]
    )
  }

}

case class DriverName(id: String, name: String)



