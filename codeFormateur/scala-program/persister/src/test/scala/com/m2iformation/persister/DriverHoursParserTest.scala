package com.m2iformation.parser

import java.io.ByteArrayInputStream

import com.m2iformation.persister.DriverNoramlizerProcessor
import org.scalatest.{FlatSpec, GivenWhenThen, Matchers}

/**
  * Created by walid_mellouli
  */
class DriverNoramlizerProcessorTest extends FlatSpec with Matchers with GivenWhenThen {

  val normalizer = new DriverNoramlizerProcessor

  it should "normalize input data" in {

    Given("Input data with valid lines")
    val namesImput =
      new ByteArrayInputStream("""
                                 |{ "id" : "10", "name": "George Vetticaden"}
                                 |{ "id" : "11", "name": "Jamie Engesser"}
                               """.stripMargin.getBytes)

    val hoursInput =
      new ByteArrayInputStream("""
                                 |<driver-hours><id>10</id><week>1</week><hours>70</hours></driver-hours>
                                 |<driver-hours><id>10</id><week>2</week><hours>70</hours></driver-hours>
                                 |<driver-hours><id>11</id><week>1</week><hours>50</hours></driver-hours>
                               """.stripMargin.getBytes)

    val milesInput =
      new ByteArrayInputStream("""
                                 |driverId,week,miles-logged
                                 |10,1,3300
                                 |10,2,3300
                                 |11,1,3000
                               """.stripMargin.getBytes)

    When("We parse input data")
    val drivers = normalizer.run(milesInput, hoursInput, namesImput)

    Then("We get all parsed lines")
    val expected: List[Driver] = List(
      Driver("10","George Vetticaden",1,70,3300),
      Driver("10","George Vetticaden",2,70,3300),
      Driver("11","Jamie Engesser",1,50,3000)
    )

    drivers should contain theSameElementsAs(expected)
  }

}
