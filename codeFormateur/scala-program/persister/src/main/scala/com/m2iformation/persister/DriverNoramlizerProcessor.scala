package com.m2iformation.persister

import java.io.InputStream

import com.m2iformation.parser.ParserBoot.serialize
import com.m2iformation.parser._
import com.m2iformation.persister.support.SerializationSupport


/**
  * Created by walid_mellouli
  */
class DriverNoramlizerProcessor extends SerializationSupport {

  //parsers
  private val milesParser = new DriverMilesParser
  private val hoursParser = new DriverHoursParser
  private val namesParser = new DriverNamesParser

  def run(milesInput: InputStream, hoursInput: InputStream, namesInput: InputStream): List[Driver] = {
    //parse input streams
    val driverMilesList: List[DriverMiles] = milesParser.parseInputStream(milesInput, skipHeader = true)
    val driverHoursList: List[DriverHours] = hoursParser.parseInputStream(hoursInput, skipHeader = false)
    val driverNameList: List[DriverName] = namesParser.parseInputStream(namesInput, skipHeader = false)

    val driversWithMiles: List[Driver] = enrichDriverWithMiles(driverNameList, driverMilesList)
    val drivers: List[Driver] = enrichDriverWithHours(driversWithMiles, driverHoursList)

    drivers
  }

  private def enrichDriverWithMiles(driverNameList: List[DriverName], driverMilesList: List[DriverMiles]): List[Driver] = {
    val driverMilesGroupedById = driverMilesList.groupBy(driverMiles => driverMiles.id)
    driverNameList.flatMap { driverName =>
      driverMilesGroupedById(driverName.id).map { driverMiles =>
        Driver(id = driverName.id, name = driverName.name, week = driverMiles.week, miles = driverMiles.miles)
      }
    }
  }

  private def enrichDriverWithHours(drivers: List[Driver], driverHoursList: List[DriverHours]): List[Driver] = {
    val driverHoursGroupedByIdWeek: Map[String, DriverHours] = driverHoursList.map(driverHours => (s"${driverHours.id}-${driverHours.week}", driverHours)).toMap
    drivers.flatMap { driver =>
      val maybeDriverHours = driverHoursGroupedByIdWeek.get(s"${driver.id}-${driver.week}")
      maybeDriverHours.map(driverHours => driver.copy(hours = driverHours.hours))
    }
  }

}
