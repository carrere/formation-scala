package com.m2iformation.service

import com.m2iformation.parser.Driver
import com.m2iformation.persister.support.SerializationSupport
import org.scalatra.{NotFound, Ok}

/**
  * Created by walid_mellouli
  */
class DriversRoute extends RestServlet with SerializationSupport {

  val inputFile = "/tmp/drivers"

  lazy val drivers = deserialize[Driver](inputFile)

  val exposedDrivers: Map[String, Option[DriverRest]] = drivers.groupBy(_.id).map{ case (id, driverList) =>
    (id, getDriverRest(driverList))
  }

  private def getDriverRest(driverList: Seq[Driver]): Option[DriverRest] = {
    driverList.headOption match {
      case Some(driver) => {
        val weeks = driverList.map(driverWeek => DriverWeek(driverWeek.week, driverWeek.hours, driverWeek.miles)).toList
        Some(DriverRest(driver.id, driver.name, weeks))
      }
      case None => None
    }
  }

  get("/drivers/id/:driverId") {

    val id = params("driverId")

    exposedDrivers.get(id) match {
      case Some(driver) => {
        Ok(driver)
      }
      case None => NotFound(s"Driver id $id not found")
    }

  }

  get("/drivers") {

    exposedDrivers.values match {
      case Nil => NotFound(s"No drivers in data base")
      case _ => Ok(exposedDrivers)
    }

  }

}

case class DriverWeek(week: Int, hours: Int, miles: Int)
case class DriverRest(id: String, name: String, weeks: List[DriverWeek])
